<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
            'name' => 'admin',
            'email' => 'admin@admin',
            'password' => Hash::make('admin'),
            'type' => 'admin',
            'remember_token' => str_random(10)
        ]);

        User::create([
            'name' => 'John Smith',
            'email' => 'john_smith@gmail.com',
            'password' => Hash::make('password'),
            'type' => 'user',
            'remember_token' => str_random(10)
        ]);
    }
}
