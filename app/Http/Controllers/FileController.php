<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\File;

class FileController extends Controller
{
    function upload(Request $request)
    {
        if($request->hasFile('file')) {
            $fileName = $request->file->getClientOriginalName();
            $request->file->storeAs('public/upload', $fileName);

            File::create([
                'name' => $fileName,
            ]);
            return redirect('/successlogin');
        }
    }

    function delete(Request $request)
    {
        Storage::delete('public/upload/' . $request->name);
        File::where('id', $request->id)->delete();
        return redirect('/successlogin');
    }
}
