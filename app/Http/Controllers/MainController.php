<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\File;

class MainController extends Controller
{
    function index()
    {
        return view('login');
    }

    function checkLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|alphaNum|min:3'
        ]);

        $user_data = array(
            'email' => $request->get('email'),
            'password' => $request->get('password')
        );

        if(Auth::attempt($user_data))
        {
            $files = File::all();
            return view('successlogin', ['files' => $files]);
        }
        else
        {
            return back()->with('error', 'Wrong Login Details');
        }
    }

    function successLogin()
    {
        $files = File::all();
            return view('successlogin', ['files' => $files]);
    }

    function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
