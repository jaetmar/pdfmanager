<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
        <!-- Styles -->

        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                height: 100%;
                justify-content: space-around;
                display: flex;
                flex-flow: column;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .horizontal {
                display: flex;
                flex-flow: row;
            }

            .vertical {
                display: flex;
                flex-flow: column;
            }

            .center {
                justify-content: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if(isset(Auth::user()->email))

                <div class="content">
                    <div class="title m-b-md">
                        {{ Auth::user()->email }}
                    </div>

                    <div>
                        @if (Auth::user()->type == 'admin')
                            <form action="{{ url('/upload') }}" enctype="multipart/form-data" method="post">
                                {{ csrf_field() }}
                                <div class="horizontal">
                                    <input type="file" name="file">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </form>
                    @endif

                    <br/>

                    @if(!$files->isEmpty())
                        <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">Name</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($files as $file)
                                        <tr>
                                            <td><a href="{{ asset('storage/upload/' . $file->name) }}">{{ $file->name }}</a></td>
                                            <td>
                                                <form action="/delete" method="POST"> 
                                                    {{ csrf_field() }}
                                                    <input type="hidden" value="{{$file->id}}" name="id">
                                                    <input type="hidden" value="{{$file->name}}" name="name">
                                                    <input class="btn btn-danger" type="submit" value="Delete"
                                                    @if (Auth::user()->type != 'admin')
                                                        disabled
                                                    @endif
                                                    >
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            No files to display
                        @endif
                    </div>

                    <div class="links">
                        <a href="{{ url('/logout') }}">Logout</a>
                    </div>
                    
                </div>

            @else
                <script>
                    window.location="/";
                </script>
            @endif
            
        </div>
    </body>
</html>